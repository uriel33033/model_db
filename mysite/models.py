# _*_ encoding: utf-8 _*_
from django.db import models


class Maker(models.Model):
    name = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

class PModel(models.Model):
    maker = models.ForeignKey(Maker, on_delete=models.CASCADE) #CASCADE 只要外鍵刪除此資料會跟著刪除
    name = models.CharField(max_length=100)
    url = models.URLField(default='http://i.imgur.com/Ous4iGB.png')

    def __str__(self):
        return self.name

class Product(models.Model):
    pmodel = models.ForeignKey(PModel, on_delete=models.CASCADE,verbose_name='型號') #改變admin後台的顯示欄位標籤名子
    nickname = models.CharField(max_length=150, default='超值二手機',verbose_name='摘要')
    description = models.TextField(default='暫無說明')
    year = models.PositiveIntegerField(default=2016,verbose_name='出廠年份')
    price = models.PositiveIntegerField(default=0,verbose_name='價格')

    def __str__(self):
        return self.nickname


class PPhoto(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    description = models.CharField(max_length=200, default='產品照片')
    url = models.URLField(default='http://i.imgur.com/Z230eeq.png')

    def __str__(self):
        return self.description
